variable "name" {
  default     = null
  description = "VPC and NAT router name."
}

variable "nat_ip_allocation" {
  default     = "AUTO_ONLY"
  description = "NAT router IP allocation mode."
}
variable "nat_ips" {
  default     = []
  description = "NAT router IP self-links."
}
variable "region" {
  default     = "us-central1"
  description = "NAT router region."
}
