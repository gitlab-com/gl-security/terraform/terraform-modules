# vpc-nat

This module creates an auto-subnetting VPC with a NAT router.

## Input Variables

Please see [variables.tf](variables.tf) for default values.

| Variable | Description |
| --- | --- |
| **`name`** | VPC and NAT router name. |
| `nat_ip_allocation` | NAT router IP allocation mode. |
| `nat_ips` | NAT router IP self-links. |
| `region` | NAT router region. |

## Output Values

| Value | Description |
| --- | --- |
| `id` | VPC ID. |

## Links

- <https://www.terraform.io/docs/providers/google/r/compute_network.html>
- <https://www.terraform.io/docs/providers/google/r/compute_router.html>
- <https://www.terraform.io/docs/providers/google/r/compute_router_nat.html>
