output "id" {
  value       = google_compute_network.default.id
  description = "VPC ID."
}
