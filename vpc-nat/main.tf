resource "google_compute_network" "default" {
  name                    = var.name
  auto_create_subnetworks = true
}

resource "google_compute_router" "default" {
  network = google_compute_network.default.id

  name   = var.name
  region = var.region
}

resource "google_compute_router_nat" "default" {
  router = google_compute_router.default.name

  name                               = var.name
  nat_ip_allocate_option             = var.nat_ip_allocation
  nat_ips                            = var.nat_ips
  region                             = var.region
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
}
