data "google_project" "default" {}

resource "google_container_cluster" "default" {
  provider = google

  # Cluster basics

  name     = var.name
  location = var.location

  release_channel {
    channel = var.release_channel
  }

  min_master_version = var.min_master_version

  # default-pool

  remove_default_node_pool = true
  initial_node_count       = 1

  node_config {
    tags = ["gke-${var.name}-node"]

    shielded_instance_config {
      enable_secure_boot = true
    }
  }

  # Automation

  maintenance_policy {
    recurring_window {
      start_time = var.maintenance_window.start
      end_time   = var.maintenance_window.end
      recurrence = var.maintenance_window.recurrence
    }
  }

  # Networking

  private_cluster_config {
    enable_private_nodes    = true
    enable_private_endpoint = false
    master_ipv4_cidr_block  = var.master_ipv4_cidr_block

    master_global_access_config {
      enabled = true
    }
  }

  network         = var.vpc_id
  networking_mode = "VPC_NATIVE"

  ip_allocation_policy {}

  enable_intranode_visibility = true

  network_policy {
    enabled = true
  }

  # Security

  enable_shielded_nodes = true

  workload_identity_config {
    workload_pool = "${data.google_project.default.project_id}.svc.id.goog"
  }

  enable_legacy_abac = false

  master_auth {
    client_certificate_config {
      issue_client_certificate = false
    }
  }

  lifecycle {
    ignore_changes = [
      maintenance_policy[0].maintenance_exclusion
    ]
  }
}

resource "google_container_node_pool" "default" {
  for_each = var.node_pools

  provider = google
  cluster  = google_container_cluster.default.name

  # Node pool details

  name     = each.key
  location = var.location

  node_count = each.value["node_count"]

  node_config {
    # Nodes

    image_type   = "COS_CONTAINERD"
    machine_type = each.value["machine_type"]
    tags         = ["gke-${var.name}-node"]

    # Security

    service_account = var.service_account

    shielded_instance_config {
      enable_secure_boot = true
    }

    # Metadata

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }

  upgrade_settings {
    max_surge       = 3
    max_unavailable = 0
  }
}
