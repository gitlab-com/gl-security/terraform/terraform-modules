output "cluster_ca_certificate" {
  value       = google_container_cluster.default.master_auth.0.cluster_ca_certificate
  description = "Base64-encoded public certificate of the cluster endpoint."
}
output "endpoint" {
  value       = google_container_cluster.default.endpoint
  description = "IP address of the cluster endpoint."
}
output "master_ipv4_cidr_block" {
  value       = var.master_ipv4_cidr_block
  description = "CIDR IP range for master network."
}
output "services_ipv4_cidr" {
  value       = google_container_cluster.default.services_ipv4_cidr
  description = "IP address range of Kubernetes services."
}
