# gke-cluster

This module creates a private GKE cluster with a globally accessible master.

The following features are always enabled:

- [Intranode visibility](https://cloud.google.com/kubernetes-engine/docs/how-to/intranode-visibility)
- [Network policy](https://cloud.google.com/kubernetes-engine/docs/how-to/network-policy)
- [Shielded nodes and secure boot](https://cloud.google.com/kubernetes-engine/docs/how-to/shielded-gke-nodes)
- [VPC-native cluster](https://cloud.google.com/kubernetes-engine/docs/how-to/alias-ips)
- [Workload Identity](https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity)

The following features are always disabled:

- Basic authentication
- Client certificate
- Legacy authorization
- Legacy endpoints

## Input Variables

Please see [variables.tf](variables.tf) for default values.

| Variable | Description |
| --- | --- |
| **`name`** | Cluster name. |
| **`service_account`** | Email address of a [least privilege](https://cloud.google.com/kubernetes-engine/docs/how-to/hardening-your-cluster#use_least_privilege_sa) GCP service account for GKE nodes. |
| `location` | Cluster location, can be a region or zone. |
| `maintenance_window` | Maintenance window attributes. |
| `master_ipv4_cidr_block` | CIDR IP range for master network. |
| `min_master_version` | Minimum version of the Kubernetes master. |
| `node_pools` | Nested map of node pool names and node count/[machine type](https://cloud.google.com/compute/docs/machine-types) (see below).<br>A regional cluster will replicate pools in multiple zones. |
| `release_channel` | GKE [release channel](https://cloud.google.com/kubernetes-engine/docs/concepts/release-channels). |
| `vpc_id` | VPC ID. |

## Output Values

| Value | Description |
| --- | --- |
| `cluster_ca_certificate` | Base64-encoded CA certificate of the cluster endpoint. |
| `endpoint` | IP address of the cluster endpoint. |
| `master_ipv4_cidr_block` | CIDR IP range for master network. |
| `services_ipv4_cidr` | IP address range of Kubernetes services. |

## Node Configuration

Example `node_pools` value with two node pools:

```
{
    pool-a = {
      node_count   = 3
      machine_type = "e2-medium"
    }
    pool-b = {
      node_count   = 3
      machine_type = "n1-standard-4"
    }
}
```

Cluster nodes are assigned network tags following the schema `gke-CLUSTERNAME-node`.

## Links

- <https://www.terraform.io/docs/providers/google/r/container_cluster.html>
- <https://www.terraform.io/docs/providers/google/r/container_node_pool.html>
