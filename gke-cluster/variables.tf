variable "name" {
  default     = null
  description = "Cluster name."
}

variable "service_account" {
  default     = null
  description = "Email address of a least privilege GCP service account for GKE nodes."
}

variable "location" {
  default     = "us-central1-a"
  description = "Cluster location, can be a region or zone."
}

variable "maintenance_window" {
  default = {
    start      = "1970-01-01T00:00:00Z"
    end        = "1970-01-02T00:00:00Z"
    recurrence = "FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR,SA,SU"
  }
  description = "Maintenance window attributes."
}

variable "master_ipv4_cidr_block" {
  default     = "172.16.0.0/28"
  description = "CIDR IP range for master network."
}

variable "min_master_version" {
  default     = ""
  description = "Minimum version of the Kubernetes master."
}

variable "node_pools" {
  default = {
    pool-a = {
      node_count   = 3
      machine_type = "e2-medium"
    }
  }
  description = "Nested map of node pool names and node count/machine type."
}

variable "release_channel" {
  default     = "STABLE"
  description = "GKE release channel."
}

variable "vpc_id" {
  default     = "default"
  description = "VPC ID."
}
